class CreatePractices < ActiveRecord::Migration[7.1]
  def change
    create_table :practices do |t|
      t.string :name
      t.string :address
      t.string :address2
      t.string :cp
      t.string :town
      t.string :phone
      t.string :phone2
      t.string :email

      t.timestamps
    end
  end
end
