class CreatePatients < ActiveRecord::Migration[7.1]
  def change
    create_table :patients do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :phone2
      t.string :address
      t.string :address2
      t.string :cp
      t.string :town
      t.string :email
      t.references :practice, null: false, foreign_key: true

      t.timestamps
    end
  end
end
