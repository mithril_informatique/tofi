class CreateAppointments < ActiveRecord::Migration[7.1]
  def change
    create_table :appointments do |t|
      t.references :practice, null: false, foreign_key: true
      t.datetime :start
      t.datetime :end
      t.string :title
      t.text :description
      t.references :practitioner, null: false, foreign_key: true
      t.references :patient, null: false, foreign_key: true

      t.timestamps
    end
  end
end
