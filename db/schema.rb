# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_04_04_103216) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appointments", force: :cascade do |t|
    t.bigint "practice_id", null: false
    t.datetime "start"
    t.datetime "end"
    t.string "title"
    t.text "description"
    t.bigint "practitioner_id", null: false
    t.bigint "patient_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["patient_id"], name: "index_appointments_on_patient_id"
    t.index ["practice_id"], name: "index_appointments_on_practice_id"
    t.index ["practitioner_id"], name: "index_appointments_on_practitioner_id"
  end

  create_table "patients", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone"
    t.string "phone2"
    t.string "address"
    t.string "address2"
    t.string "cp"
    t.string "town"
    t.string "email"
    t.bigint "practice_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["practice_id"], name: "index_patients_on_practice_id"
  end

  create_table "practices", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "address2"
    t.string "cp"
    t.string "town"
    t.string "phone"
    t.string "phone2"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "practitioners", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone"
    t.string "email"
    t.bigint "practice_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["practice_id"], name: "index_practitioners_on_practice_id"
  end

  add_foreign_key "appointments", "patients"
  add_foreign_key "appointments", "practices"
  add_foreign_key "appointments", "practitioners"
  add_foreign_key "patients", "practices"
  add_foreign_key "practitioners", "practices"
end
