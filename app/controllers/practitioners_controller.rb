class PractitionersController < ApplicationController
  before_action :set_practitioner, only: %i[ show edit update destroy ]

  # GET /practitioners
  def index
    @practitioners = Practitioner.all
  end

  # GET /practitioners/1
  def show
  end

  # GET /practitioners/new
  def new
    @practitioner = Practitioner.new
  end

  # GET /practitioners/1/edit
  def edit
  end

  # POST /practitioners
  def create
    @practitioner = Practitioner.new(practitioner_params)

    if @practitioner.save
      redirect_to @practitioner, notice: "Practitioner was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /practitioners/1
  def update
    if @practitioner.update(practitioner_params)
      redirect_to @practitioner, notice: "Practitioner was successfully updated.", status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  # DELETE /practitioners/1
  def destroy
    @practitioner.destroy!
    redirect_to practitioners_url, notice: "Practitioner was successfully destroyed.", status: :see_other
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_practitioner
      @practitioner = Practitioner.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def practitioner_params
      params.require(:practitioner).permit(:first_name, :last_name, :phone, :email, :practice_id)
    end
end
