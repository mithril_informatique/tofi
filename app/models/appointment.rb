class Appointment < ApplicationRecord
  belongs_to :practice
  belongs_to :practitioner
  belongs_to :patient
end
